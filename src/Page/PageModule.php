<?php
namespace App\Page;

use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class PageModule
{
    private $renderer;
    public function __construct(Router $router, RendererInterface $renderer)
    {
        $this->renderer = $renderer;
        $this->renderer->addPath('page', __DIR__ .'/views');
        $router->get('/', [$this,'index'], 'page.index');
    }

    public function index(Request $request): string
    {
        return $this->renderer->render('@page/index');
    }

}
