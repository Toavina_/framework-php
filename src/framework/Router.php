<?php
namespace Framework;

use Framework\Middleware\CallableMiddleware;
use Framework\Router\Route;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\FastRouteRouter;
use Zend\Expressive\Router\Route as ZendRoute;

/**
 * Register and match routes
 */
class Router
{

    /**
     * @var FastRouteRouter
     */
    private $router;

    public function __construct()
    {
        $this->router = new FastRouteRouter();
    }

    /**
     * GET method route
     *
     * @param string $path
     * @param callable $callable
     * @param string $routename
     * @return void
     */
    public function get(string $path, $callable, string $routename)
    {
        $this->router->addRoute(new ZendRoute($path, new CallableMiddleware($callable), ['GET'], $routename));
    }

    /**
     *  Match route
     *
     * @param ServerRequestInterface $request
     * @return Route|null
     */
    public function match(ServerRequestInterface $request): ?Route
    {
        $result = $this->router->match($request);
        if ($result->getMatchedRoute()) {
            return new Route(
                $result->getMatchedRouteName(),
                $result->getMatchedRoute()->getMiddleware()->getCallable(),
                $result->getMatchedParams()
            );
        }
        return null;
    }

    /**
     * Generate uri from parameters
     *
     * @param string $routename
     * @param array $params
     * @return string
     */
    public function generateUri(string $routename, array $params): string
    {
        return $this->router->generateUri($routename, $params);
    }
}
