<?php
namespace Framework\Router;

/**
 * Represente each match app route
 */
class Route
{
    private $name;
    private $callable;
    private $parameters;
    public function __construct(string $routename, callable $callable, array $parameters)
    {
        $this->name = $routename;
        $this->callable = $callable;
        $this->parameters = $parameters;
    }
    /**
     * get Route name
     *
     * @return string
     */
    public function getName():string
    {
        return $this->name;
    }

    /**
     * get Route callable
     * @return callable
     */

    public function getCallback():callable
    {
        return $this->callable;
    }

    /**
     * get url parameter
     * @return string[] List parameter
     */
    public function getParameters():array
    {
        return $this->parameters;
    }
}
