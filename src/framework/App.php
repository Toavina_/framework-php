<?php
namespace Framework;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class App
{
    private $modules = [];
    private $router;

    public function __construct(array $modules, array $dependencies = [])
    {
        $this->router = new Router();
        if (array_key_exists('renderer', $dependencies)) {
            $dependencies['renderer']->addGlobal('router', $this->router);
        }
        foreach ($modules as $module) {
            $this->modules = new $module($this->router, $dependencies['renderer']);
        }
    }

    public function run(ServerRequestInterface $request): ResponseInterface
    {
        $uri = $request->getUri()->getPath();
       
        if (!empty($uri) && $uri[-1] === "/" && $uri !== "/") {
            $reponse = (new Response())
            ->withHeader('Location', substr($uri, 0, -1))
            ->withStatus(301);
            return $reponse;
        }
        
        $route = $this->router->match($request);
        if (is_null($route)) {
            return  new Response(404, [], "Page introuvable");
        }
        /**
         * Send attibute to ServerRequest
         */
        
        $params = $route->getParameters();
        foreach ($params as $key => $param) {
            $request = $request->withAttribute($key, $param);
        }
        /**
         * Call callback if matched route
         */
        $reponse = call_user_func_array($route->getCallback(), [$request]);

        if (is_string($reponse)) {
            return new Response(200, [], $reponse);
        } elseif ($reponse instanceof ResponseInterface) {
            return $reponse;
        } else {
            throw new \Exception("Response is not a string or instance of ResponseInterface");
        }
    }
}
