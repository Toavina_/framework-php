<?php
namespace Tests\Framework;

use Framework\Renderer;
use \Framework\Renderer\PHPRenderer;
use PHPUnit\Framework\TestCase;

class PHPRendererTest extends TestCase
{
    private $renderer;

    public function setUp():void
    {
        $this->renderer = new PHPRenderer(__DIR__ . '/views');
    }

    public function testRenderTheRightPath()
    {
        $this->renderer->addPath('blog',  __DIR__ . '/views');
        $content = $this->renderer->render('@blog/demo');
        $this->assertEquals('Hello view',$content);
    }


    public function testRenderTheDefaultPath()
    {
        $content = $this->renderer->render('demo');
        $this->assertEquals('Hello view',$content);
    }

    public function testRenderTheRightPathWithParameters()
    {
        $this->renderer->addPath('blog',  __DIR__ . '/views');
        $content = $this->renderer->render('@blog/demoparams',['demo'=>'test']);
        $this->assertEquals('Hello view test',$content);
    }

    public function testAddGlobalParameter()
    {
        $this->renderer->addGlobal('demo','Marc');
        $content = $this->renderer->render('demoparams');
        $this->assertEquals('Hello view Marc',$content);
    }
}