<?php
namespace Tests\Framework;

use \App\Blog\BlogModule;
use \Framework\App;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use \Tests\Framework\Module\ErroredModule;
use \Tests\Framework\Module\StringModule;

class AppTest extends TestCase{

    public function testRedirectTrailingSlash(){
        $request = new ServerRequest('GET','/blog/');
        $app = new App([
            BlogModule::class
        ]);
        $response = $app->run($request);
        $this->assertContains('/blog',$response->getHeader('Location'));
        $this->assertEquals(301,$response->getStatusCode());
    }

    public function testBlog(){
        $request = new ServerRequest('GET','/blog');
        $requestSingle = new ServerRequest('GET','/blog/article-de-test');
        $app = new App([
           BlogModule::class
        ]);
        $response = $app->run($request);
        $responseSingle = $app->run($requestSingle);
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertInstanceOf(ResponseInterface::class, $responseSingle);
        $this->assertStringContainsString('<h1>Bienvenue sur l\'article: article-de-test</h1>',(string) $responseSingle->getBody());
        $this->assertStringContainsString('<h1>Bienvenue sur le blog</h1>',(string) $response->getBody());
        $this->assertEquals(200,$response->getStatusCode());
    }

    public function testThrowExceptionIfNoResponseSend(){
        $request = new ServerRequest('GET','/demo');
        $app = new App([
           ErroredModule::class
        ]);
        $this->expectException(\Exception::class);
        $response = $app->run($request);

    }

    public function testConvertResponseToString(){
        $request = new ServerRequest('GET','/demo');
        $app = new App([
           StringModule::class
        ]);
        $response = $app->run($request);
        $this->assertEquals('DEMO',$response->getBody());
    }

    public function testError404(){
        $request = new ServerRequest('GET','/dfdf');
        $app = new App([
            BlogModule::class
        ]);
        $response = $app->run($request);
        $this->assertStringContainsString('Page introuvable',(string) $response->getBody());
        $this->assertEquals(404,$response->getStatusCode());
    }
}