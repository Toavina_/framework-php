<?php
require '../vendor/autoload.php';

use App\Blog\BlogModule;
use App\Page\PageModule;
use Framework\App;
use Framework\Renderer\PHPRenderer;
use Framework\Renderer\TwigRenderer;
use GuzzleHttp\Psr7\ServerRequest;

$renderer = new TwigRenderer(dirname(__DIR__) .'/views');


$app = new App([
    PageModule::class,
    BlogModule::class
], [
    'renderer'=>$renderer
]);
$response = $app->run(ServerRequest::fromGlobals());
\Http\Response\send($response);
